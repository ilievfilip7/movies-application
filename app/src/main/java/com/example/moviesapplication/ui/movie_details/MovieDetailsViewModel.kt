package com.example.moviesapplication.ui.movie_details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviesapplication.database.FavouriteMovie
import com.example.moviesapplication.database.MovieDatabaseDao
import com.example.moviesapplication.model.Movie
import com.example.moviesapplication.network.MoviesAPI
import kotlinx.coroutines.*

/**
 * View model for the movie details activity.
 */
class MovieDetailsViewModel(
    private val favouriteMovie: FavouriteMovie,
    val database: MovieDatabaseDao
) : ViewModel() {

    private val _listOfSimilarMovies: MutableLiveData<ArrayList<Movie>> =
        MutableLiveData<ArrayList<Movie>>()
    val listOfSimilarMovies: LiveData<ArrayList<Movie>> = _listOfSimilarMovies

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)
    val isMovieAddedToFavourites = MutableLiveData<Boolean>()

    init {
        coroutineScope.launch {
            isMovieAddedToFavourites.value = isFavourite(favouriteMovie.movieId)
        }
    }

    fun getSimilarMovies(id: Int) {
        coroutineScope.launch {
            val getPropertiesDeferred =
                MoviesAPI.retrofitService.getSimilarMoviesAsync(id = id)
            try {
                val result = getPropertiesDeferred.await()
                _listOfSimilarMovies.value = result.results as ArrayList<Movie>
            } catch (t: Throwable) {
                _listOfSimilarMovies.value = ArrayList()
                println(t.message)
            }
        }
    }

    fun addOrDeleteMovieFromFavorites(id: Long) {
        coroutineScope.launch {
            isMovieAddedToFavourites.value = !isFavourite(id)
            withContext(Dispatchers.IO) {
                if (isMovieAddedToFavourites.value!!) {
                    database.insert(favouriteMovie)
                } else {
                    database.deleteMovie(id)
                }
            }
        }
    }

    private suspend fun isFavourite(id: Long): Boolean {
        return withContext(Dispatchers.IO) {
            database.isFavourite(id)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}