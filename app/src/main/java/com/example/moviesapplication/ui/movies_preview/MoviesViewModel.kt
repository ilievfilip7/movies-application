package com.example.moviesapplication.ui.movies_preview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviesapplication.model.Movie
import com.example.moviesapplication.network.MoviesAPI
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

/**
 * The API response status.
 */
enum class MovieApiStatus {
    /** API request is in loading state. */
    LOADING,

    /** API request is in error state. */
    ERROR,

    /** API request is in success state. */
    SUCCESS
}

/**
 * View model for the movie activity.
 */
class MoviesViewModel : ViewModel() {

    private val _listOfPopularMovies: MutableLiveData<ArrayList<Movie>> =
        MutableLiveData<ArrayList<Movie>>()
    val listOfPopularMovies: LiveData<ArrayList<Movie>> = _listOfPopularMovies

    private val _listOfTopRatedMovies: MutableLiveData<ArrayList<Movie>> =
        MutableLiveData<ArrayList<Movie>>()
    val listOfTopRatedMovies: LiveData<ArrayList<Movie>> = _listOfTopRatedMovies

    private val _listOfNowPlayingMovies: MutableLiveData<ArrayList<Movie>> =
        MutableLiveData<ArrayList<Movie>>()
    val listOfNowPlayingMovies: LiveData<ArrayList<Movie>> = _listOfNowPlayingMovies

    private val _listOfUpcomingMovies: MutableLiveData<ArrayList<Movie>> =
        MutableLiveData<ArrayList<Movie>>()
    val listOfUpcomingMovies: LiveData<ArrayList<Movie>> = _listOfUpcomingMovies

    private val _listOfSearchedMovies: MutableLiveData<ArrayList<Movie>> =
        MutableLiveData<ArrayList<Movie>>()
    val listOfSearchedMovies: LiveData<ArrayList<Movie>> = _listOfSearchedMovies

    private val _status = MutableLiveData<MovieApiStatus>()
    val status: LiveData<MovieApiStatus>
        get() = _status

    private var mPopularMoviesPageNumber: Int = 1
    private var mTopRatedMoviesPageNumber: Int = 1
    private var mNowPlayingMoviesPageNumber: Int = 1
    private var mUpcomingMoviesPageNumber: Int = 1
    private var mSearchedMoviesPageNumber: Int = 1

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        getPopularMovies()
        getTopRatedMovies()
        getNowPlayingMovies()
        getUpcomingMovies()
    }

    fun getPopularMovies() {
        coroutineScope.launch {
            val getPropertiesDeferred =
                MoviesAPI.retrofitService.getPopularMoviesAsync(page = mPopularMoviesPageNumber)
            try {
                _status.value = MovieApiStatus.LOADING
                val result = getPropertiesDeferred.await()
                _listOfPopularMovies.value = if (mPopularMoviesPageNumber == 1) {
                    result.results as ArrayList<Movie>
                } else {
                    _listOfPopularMovies.value?.plus(result.results) as ArrayList<Movie>
                }
                _status.value = MovieApiStatus.SUCCESS
                mPopularMoviesPageNumber += 1
            } catch (t: Throwable) {
                _status.value = MovieApiStatus.ERROR
                _listOfPopularMovies.value = ArrayList()
                println(t.message)
            }
        }
    }

    fun getTopRatedMovies() {
        coroutineScope.launch {
            val getPropertiesDeferred =
                MoviesAPI.retrofitService.getTopRatedMoviesAsync(page = mTopRatedMoviesPageNumber)
            try {
                val result = getPropertiesDeferred.await()
                _listOfTopRatedMovies.value = if (mTopRatedMoviesPageNumber == 1) {
                    result.results as ArrayList<Movie>
                } else {
                    _listOfTopRatedMovies.value?.plus(result.results) as ArrayList<Movie>
                }
                mTopRatedMoviesPageNumber += 1
            } catch (t: Throwable) {
                _listOfTopRatedMovies.value = ArrayList()
                println(t.message)
            }
        }
    }


    fun getNowPlayingMovies() {
        coroutineScope.launch {
            val getPropertiesDeferred =
                MoviesAPI.retrofitService.getNowPlayingMoviesAsync(page = mNowPlayingMoviesPageNumber)
            try {
                val result = getPropertiesDeferred.await()
                _listOfNowPlayingMovies.value = if (mNowPlayingMoviesPageNumber == 1) {
                    result.results as ArrayList<Movie>
                } else {
                    _listOfNowPlayingMovies.value?.plus(result.results) as ArrayList<Movie>
                }
                mNowPlayingMoviesPageNumber += 1
            } catch (t: Throwable) {
                _listOfNowPlayingMovies.value = ArrayList()
                println(t.message)
            }
        }
    }

    fun getUpcomingMovies() {
        coroutineScope.launch {
            val getPropertiesDeferred =
                MoviesAPI.retrofitService.getUpcomingMoviesAsync(page = mUpcomingMoviesPageNumber)
            try {
                val result = getPropertiesDeferred.await()
                _listOfUpcomingMovies.value = if (mUpcomingMoviesPageNumber == 1) {
                    result.results as ArrayList<Movie>
                } else {
                    _listOfUpcomingMovies.value?.plus(result.results) as ArrayList<Movie>
                }
                mUpcomingMoviesPageNumber += 1
            } catch (t: Throwable) {
                _listOfUpcomingMovies.value = ArrayList()
                println(t.message)
            }
        }
    }

    fun getMoviesByTitle(query: String?) {
        coroutineScope.launch {
            val getPropertiesDeferred = MoviesAPI.retrofitService.getMoviesByTitleAsync(
                query = query ?: "",
                page = mSearchedMoviesPageNumber
            )
            try {
                val result = getPropertiesDeferred.await()
                _listOfSearchedMovies.value = if (mSearchedMoviesPageNumber == 1) {
                    result.results as ArrayList<Movie>
                } else {
                    _listOfSearchedMovies.value?.plus(result.results) as ArrayList<Movie>
                }
                mSearchedMoviesPageNumber += 1
            } catch (t: Throwable) {
                _listOfSearchedMovies.value = ArrayList()
                println(t.message)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}